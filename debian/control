Source: ocamlbricks
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Lucas Nussbaum <lucas@debian.org>
Build-Depends: bash (>= 3.1),
               bzr,
               camlp4-extra,
               debhelper (>= 9),
               dh-ocaml (>= 0.9.1),
               liblablgtk2-ocaml-dev (>= 2.12.0-2),
               ocaml-findlib,
               ocaml-nox (>= 3.12.1),
               ocamlbuild
Standards-Version: 4.1.1
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocamlbricks
Vcs-Git: https://salsa.debian.org/ocaml-team/ocamlbricks.git
Homepage: https://launchpad.net/ocamlbricks

Package: libocamlbricks-ocaml-dev
Architecture: any
Depends: liblablgtk2-ocaml-dev (>= 2.12.0-2),
         ${misc:Depends},
         ${ocaml:Depends},
         ${shlibs:Depends}
Provides: ${ocaml:Provides}
Replaces: ocamlbricks (<< 0.50.1)
Breaks: marionnet (<< 0.90.6+bzr506)
Suggests: ocaml-findlib
Description: Miscellaneous utility functions in OCaml for Marionnet
 This OCaml library provides a set of needed and useful macros for developing.
 It is mainly used by the marionnet package.
 Modules and functionality are the following :
 .
  - Configuration_files: Allow to get information from configuration files
  - Environments: Environments are useful for maintaining the state, intendend
 as a set of bindings, of a user interaction with a GUI
  - FilenameExtra: Additional features for the standard module Filename
  - Fix: Poor man fix point operators
  - Hashm[m]ap: 2 Module implementing polymorphic unbounded (multi) maps
  - Hashset: Very simple module implementing a polymorphic unbounded sets
  - Identifier: Build and manage unique (fresh) identifiers
  - Ledgrid: Constants Some global constant definitions, for fine-tuning
  - ListExtra: Additional features for the standard module List
  - Memo: Module for building memoised functions
  - Oomarshal: Object-oriented marshalling support
  - PreludeExtra: "Additional features" for the standard module Pervasives
  - Shell: A collection of (mainly quick and easy) wrappers for the most
 famous Unix tools and generic unix commands or scripts
  - StrExtra: Additional features for the standard library Str
  - Sugar: Basic shortcuts and syntactic sugar
  - SysExtra: Additional features for the standard module Sys
  - UnixExtra: Additional features for the standard library Unix
  - Widget: Some generic tools for building GUIs
  - Wrapper: Handling shell scripts in OCaml
